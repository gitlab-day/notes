# Conectar ao GitLab remote via SSH

## Procurar chaves existentes

```bash
ls -al ~/.ssh
```
## Criar nova chave

```bash
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

Resposta:

```bash
Generating public/private rsa key pair.
```

Pressione `enter` para gerar as chaves com o nome padrão `id_rsa`:

```bash
Enter a file in which to save the key (/Users/you/.ssh/id_rsa): [Press enter]
```

Defina uma senha para a chave:

```bash
Enter passphrase (empty for no passphrase): [digite a senha]
Enter same passphrase again: [digite a senha novamente]
```

Adicione a chave ao seu sistema:

```bash
ssh-add ~/.ssh/id_rsa
```

Copie a chave pública para adicionar ao GitLab:

```bash
pbcopy < ~/.ssh/id_rsa.pub
```

## Adicione ao GitLab

Navegue no seu **Profile > Settings > SSH keys**

Cole a chave na caixa de texto e atribua um nome a ela, se necessário.

## Teste a conexão

```bash
ssh -T git@gitlab.com
```

## Gerenciar chaves para múltiplos servidores

Caso vc já tenha chaves existentes, ou tenha outras conexões com outros servidores Git,
dê um nome diferente à chave.

No passo para nomear a chave:

```bash
Enter a file in which to save the key (/Users/you/.ssh/id_rsa): [Press enter]
```

Dê o nome que desejar ao arquivo:

```bash
/Users/you/.ssh/id_rsa1
```

Neste caso, será [necessário adioná-la à suas configurações](https://docs.gitlab.com/ce/ssh/README.html#working-with-non-default-ssh-key-pair-paths)
(`~/.ssh/config`):

```config
# GitLab.com server
Host gitlab.com
RSAAuthentication yes
IdentityFile ~/.ssh/id_rsa

# Private GitLab server
Host gitlab.company.com
RSAAuthentication yes
IdentityFile ~/.ssh/id_rsa1
```

Teste ambas conexões:

```bash
ssh -T git@gitlab.com
```

```bash
ssh -T git@gitlab.company.com
```










