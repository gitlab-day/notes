## Termos e comandos básicos em Git

### Remote / Local / Upstream

O repositório "remoto" é a cópia dos arquivos do projeto que existe no servidor
remoto. O repo local, é a cópia dos arquivos do projeto na sua máquina.

No caso do projeto ser um "fork" ou um "mirror", o repo de origem é o
"upstream", seu fork é o "remoto", e seu computador é a copia "local". 

### Untracked files

Aquivos não-tracked "Untracked files": novos arquivos que o Git ainda não identificou:

  - (1) `git status`:

    ```bash
    On branch test
    Untracked files:
      (use "git add <file>..." to include in what will be committed)

      slide31.md

    nothing added to commit but untracked files present (use "git add" to track)
    ```

Identifique-os com `git add`:

  - (2) `git add <file-name>` ou `git add --all` ou `git add .`:

    ```bash
    ➜  gitlab-day git:(test) git status
    On branch test
    Changes to be committed:
      (use "git reset HEAD <file>..." to unstage)

      new file:   slide31.md
     ```

### Working area

Os arquivos adicionados com `git add` estão agora na área de trabalho "Working area":
arquivos que foram modificados mas não-committed:

  - (1) `git status`:

    ```bash
    On branch test
    Changes to be committed:
      (use "git reset HEAD <file>..." to unstage)

      new file:   slide31.md
    ```

Mande-os para a área de preparo com `git commit`:

  - (2) `git commit -m "add slide 31 prep"`

    ```bash
    ➜  gitlab-day git:(test) ✗ git commit -m "add slide 31 prep"
     [test 29ee913] add slide 31 prep
     1 file changed, 31 insertions(+)
     create mode 100644 slide31.md
     ```

### Staging area

Os arquivos preparados estão agora na área de preparo:

  - (1) `git status`

    ```bash
    ➜  gitlab-day git:(test) ✗ git status
    On branch test
    nothing to commit, working tree clean
    ```

### Upload para o servidor

Os aquivos estão prontos para serem enviados para o servidor
(repo remoto). Vamos enviá-los com o comando `git push`:

  - (1) `git push origin test`

    ```bash
    ➜  gitlab-day git:(test) git push origin test
    Counting objects: 3, done.
    Delta compression using up to 4 threads.
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (3/3), 708 bytes | 708.00 KiB/s, done.
    Total 3 (delta 1), reused 0 (delta 0)
    remote:
    remote: To create a merge request for test, visit:
    remote:   http://demo.tanuki.cloud/marcia/gitlab-day/merge_requests/new?merge_request%5Bsource_branch%5D=test
    remote:
    To demo.tanuki.cloud:marcia/gitlab-day.git
     * [new branch]      test -> test
    ```

### Download do servidor

Caso o repo remoto receba novos commits, é necessário atualizar seu repo local.
O comando `git pull` fará o download das alterações que estão branch remoto:

```bash
➜  gitlab-day git:(test) ✗ git pull origin test
From demo.tanuki.cloud:marcia/gitlab-day
 * branch            test       -> FETCH_HEAD
Auto-merging slide31.md
Merge made by the 'recursive' strategy.
 slide31.md | 17 +++++++----------
 1 file changed, 7 insertions(+), 10 deletions(-)
```
