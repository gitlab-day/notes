# GitLab Day - São Paulo - Brasil - 2018

## Referências

### Git

<https://git-scm.com/>

#### Introducão ao Git

- [Controle de versão (Português)](https://git-scm.com/book/pt-br/v1/Primeiros-passos-Sobre-Controle-de-Vers%C3%A3o)
- [Guia prático (Português)](http://rogerdudler.github.io/git-guide/index.pt_BR.html)
- [Git Cheatsheet (English)](https://gitlab.com/gitlab-com/marketing/general/blob/master/design/publications/git-cheatsheet/print-pdf/git-cheatsheet.pdf)
- [Introductory video (English)](https://git-scm.com/video/what-is-git)
- [GitLab's Git documentation (English)](https://docs.gitlab.com/ee/topics/git/)

#### Git-CLI

Git-CLI software são aplicatibos de terceitos que ajudam a trabalhar em platarformas Git com elementos visuais:

- [GitKraken](https://www.gitkraken.com/)
    - [Integration with GitLab](https://support.gitkraken.com/integrations/gitlab)
- [Tower](https://www.git-tower.com)
    - [Integration with GitLab](https://about.gitlab.com/2015/11/10/gitlab-and-tower-2-dot-3/)
- [GitHub Desktop Application](https://desktop.github.com/)
    - [How to integrate](https://community.reclaimhosting.com/t/using-github-desktop-with-gitlab/876)

#### Instalar Git

- [Install Git - Linux, macOS, Windows (English)](https://linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/)

#### Debugging Git

- [Dicas e truques - Tricks and Tips (English)](https://about.gitlab.com/2016/12/08/git-tips-and-tricks/)
- [Como desfazer coisas em Git (English)](https://docs.gitlab.com/ee/topics/git/numerous_undo_possibilities_in_git/)

### Introdução ao GitLab

[Conteúdo básico (English)](htpps://docs.gitlab.com/ee/university/)

#### Sobre o GitLab

- [Website (English)](https://about.gitlab.com/)
  - [Blog (English)](https://about.gitlab.com/blog/)
  - [Cultura da empresa (English)](https://about.gitlab.com/culture/)
- [Documentação do GitLab (English)](https://docs.gitlab.com/)
  - [Ferramenta de busca](https://docs.gitlab.com/search/)
- [Aplicativos de terceiros](https://about.gitlab.com/applications/)

#### Workflow

- [Conectar ao GitLab via SSH (Português)](https://demo.tanuki.cloud/marcia/gitlab-day/blob/master/ssh-key-pair.md)
- [Permissões do usuário (English)](https://docs.gitlab.com/ee/user/permissions.html)
- [GitLab Workflow, an overview](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/)

(TBA)

#### Markdown

- [Aprenda markdown (Português)](https://blog.da2k.com.br/2015/02/08/aprenda-markdown/)
- [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)

#### Editores de código

- [Sublime Text](https://www.sublimetext.com/)
- [Atom](https://atom.io/)
- [Brakets](http://brackets.io/)
- [Adobe Dreamweaver](https://www.adobe.com/br/products/dreamweaver.html)

#### GitLab CI/CD

- [Webpage CI/CD (English)](https://about.gitlab.com/features/gitlab-ci-cd/)
- [Introdução a Continuous Integration, Delivery, and Deployment (English)](https://about.gitlab.com/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/)
- [Exemplos de GitLab CI/CD (English)](https://docs.gitlab.com/ee/ci/examples/README.html)
